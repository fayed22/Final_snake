var size,p=0;
var fieldSize = 13;
var tileSize = 76;
var tileArray = [];
var globezLayer;
var initialx=300,initialy=30;
var BackGroundWidth,BackGroundHeight;
var snakeHeadX,snakeHeadY;
var SnakeArray = null,SnakeArrayTwo= null; 
var SnakeFood = null; 
var spriteBackGround = null; 
var BackGroundPos;
var Direction ="";
var scl=0.10,lllllllll;
var charecter;
var food = 0,coin=0,snk=0; 
var left,right,up,down;
var jumpup=1,jumpdown=11,jumpleft=9,jumpright=3; 
var cellWidth = 30,cellHeight=30;
var Enum = {snakecell:0, snakefood:1 , background:2,snakecoin:3,snakecharecter:4,bomb:5,dragoncell:6};
var snakeJSGameLayer = null;
var snakeLength = 3; //Length of the snake
var ScoreLabel = null;
var GameOverLabel = null;
var bCollisionDetected = false;
var p=0,q=0,o=2,m,x,y;
var r,b;
var that;
var flag=true;
var bombSprite,bombCreated=false;
var startTime,endTime,touchTime;
var count=10;
var deletSpriteSnakeCell,deletDragonCell;
var spriteDragonCellLast   

var SpriteBackGround = cc.Sprite.extend({
 ctor: function(texture, rect) 
 {           
    this._super(texture, rect);
 }
});
//Snake cell class 
var SpriteSnakeCell = cc.Sprite.extend({
    ctor: function(texture) 
    {           
        this._super(texture);
    }
});
//Snake food class 
var SpriteSnakeFood = cc.Sprite.extend({
    ctor: function(texture) {           
    this._super(texture);
 }
});

var SpriteSnakeBomb = cc.Sprite.extend({
    ctor: function(texture) {           
    this._super(texture);
 }
});
var HelloWorldLayer = cc.Layer.extend({
    sprite:null,
    ctor:function () {
        this._super();
        that=this;
        size = cc.winSize;
        var background=new cc.Sprite.create("res/bakground.png");
        this.addChild(background);
        background.setPosition(0,0);
        background.setScale(2);

        FoodLabel= new cc.LabelTTF("Food : 0","Arial",32);
        FoodLabel.x=size.width/2+250;
        FoodLabel.y=size.height/2+210;
        FoodLabel.setColor(0,0,0);
        this.addChild(FoodLabel,5);
        FoodLabel.visible=true;

        CoinLabel= new cc.LabelTTF("Coin : 0","Arial",32);
        CoinLabel.x=size.width/2+250;
        CoinLabel.y=size.height/2+260;
        CoinLabel.setColor(0,0,0);
        this.addChild(CoinLabel,5);
        CoinLabel.visible=true;

        GameOverLabel = new cc.LabelTTF("Game Over!!!!!", "Arial", 38); 
        GameOverLabel.x = size.width / 2;
        GameOverLabel.y = size.height / 2;  
        GameOverLabel.setColor(255,0,0);
        this.addChild(GameOverLabel, 5); 
        GameOverLabel.visible = false;
        this.createSnake();
        this.createFood();
        this.createCharacter();
        for(var i=0;i<11;i++)
        {
            for(var j=0;j<11;j++)
            {
                p++;
                if(j%2==0)
                {
                    sprite=new cc.Sprite.create("res/1.png");
                    this.addChild(sprite);
                    //sprite.setScale(0.6);
                    sprite.setPosition((initialx+(j*50)-(i*25)),(initialy+(j*12)+(i*40)));
                }
                else
                {
                    sprite=new cc.Sprite.create("res/2.png");
                    this.addChild(sprite);
                    //sprite.setScale(0.6);
                    sprite.setPosition((initialx+(j*50)-(i*25)),(initialy+(j*12)+(i*40)));
                }
                //tileArray[i][j]=p;
            }
        }
        if ('keyboard' in cc.sys.capabilities) {
            cc.eventManager.addListener({
                event: cc.EventListener.KEYBOARD,
                onKeyPressed: function (key, event) {
                   var target = event.getCurrentTarget();
                   if(key == "37" && direction != "right") direction = "left";
                   else if(key == "38" && direction != "down") direction = "up";
                   else if(key == "39" && direction != "left") direction = "right";
                   else if(key == "40" && direction != "up") direction = "down";
                 } 
            }, this);
        } else {
            cc.log("KEYBOARD Not supported");
        }
        this.schedule(this.CallForBomb,15);
        this.schedule(this.gameLoop,1);
        

        return true;
    },
    createSnake:function()
    {
        direction="right";
        //count++;
        if(( typeof SnakeArray != 'undefined' && SnakeArray instanceof Array ) && SnakeArray.length > 0 )
        {                
            for(var i = 0; i< SnakeArray.length; i++)             
            {
                this.removeChild(SnakeArray[i],true);                   
            }
        } 
        SnakeArray = []; 
        SnakeArrayTwo = [];  
        //lllllllll=[];
        var elmsToRemove = SnakeArray.length - length;
        if(elmsToRemove>1)
        {
            SnakeArray.splice(snakeLength-1,elmsToRemove);
        }
        //for(var i=0;i<snakeLength;i++)
        for(var i = snakeLength-1; i>=0; i--)
        {
           
           count++;
           var spriteSnakeCell = new SpriteSnakeCell("res/red.png");
           var spriteDragonCell= new SpriteSnakeCell("res/d1.png");
           //spriteSnakeCell.setScale(0.05);
           //spriteSnakeCell.setAnchorPoint(0,0);
           spriteSnakeCell.setTag(Enum.snakecell);  
           spriteDragonCell.setTag(Enum.dragoncell);        
           var xMov = (initialx+(o*50));
           var yMov = (initialy+(o*12));
           spriteSnakeCell.x = xMov;
           spriteSnakeCell.y = yMov;
           spriteDragonCell.x= xMov;
           spriteDragonCell.y= yMov;
           this.addChild(spriteSnakeCell,2); 
           this.addChild(spriteDragonCell,2);
           // lllllllll[i]={
           //  tag : count
           // };          
           SnakeArray.push(spriteSnakeCell);
           SnakeArrayTwo.push(spriteDragonCell);
           cc.log(spriteDragonCell.x,spriteDragonCell.y);
           o--; 
        }
        
    },
    getRandomArbitrary:function(min,max) 
    {
        return Math.floor(Math.random() * (max - min) + min);
    },
    CallForBomb:function()
    {
        this.createBomb();
        setTimeout(function(){
            Bomb.x=0;
            Bomb.y=0;
            cc.log("tag of bomb "+bombSprite.getTag());
            that.removeChildByTag(Enum.bomb,true);
        },10000);
    },
    createBomb:function()
    {
        
        b=1;
        bombCreated=true;
        bomb=true;
        var p=new Date();
        startTime=p.getSeconds();
        endTime=startTime+10;
        // if(endTime>60)
        // {
        //     endTime=(60-startTime)+(endTime-60);
        // }
        cc.log("start "+startTime);
        cc.log("End "+endTime);
        if(this.getChildByTag(Enum.bomb)!=null)
        {
            this.removeChildByTag(Enum.bomb,true); 
        } 
        bombSprite= new SpriteSnakeBomb("res/bomb.png");
        bombSprite.setTag(Enum.bomb);
        this.addChild(bombSprite,3);
        x=this.getRandomArbitrary(0,11);
        y=this.getRandomArbitrary(0,11);
        bombSprite.setPosition((initialx+(y*50)-(x*25)),(initialy+(y*12)+(x*40)));
        Bomb = {
            x: (initialx+(y*50)-(x*25)) , 
            y: (initialy+(y*12)+(x*40))  
        };
        for(i=0;i<SnakeArray.length;i++)
        {
            var xxxxxx=SnakeArray[i].x;
            var yyyyyy=SnakeArray[i].y;
            if(xxxxxx==Bomb.x&&yyyyyy==Bomb.y)
            {
                cc.log("change");
                var x=this.getRandomArbitrary(0,11);
                var y=this.getRandomArbitrary(0,11);
                bombSprite.setPosition((initialx+(y*50)-(x*25)),(initialy+(y*12)+(x*40)));
                Bomb = {
                    x: (initialx+(y*50)-(x*25)) , 
                    y: (initialy+(y*12)+(x*40))  
                };
            }
        }

    },
    BombCount:function()
    {
        var m=((endTime-touchTime)*1000);
        this.schedule(this.correction,1.0);
        cc.log("m is"+m);
        setTimeout(function(){
            //bombCreated=false;
            that.unscheduleCount();
            
            //bomb=false;
        },m);
        
    },
    correction:function()
    {
        b++;
        cc.log("b is"+b);
    },
    unscheduleCount:function()
    {
           
        cc.log("length"+SnakeArray.length);
        cc.log("value of b "+b);
        if(SnakeArray.length>b)
        {
            
            cc.log("no of snake to be deleted "+((SnakeArray.length)-b));
            var k=SnakeArray.length;
            for(var j=b;j<k;j++)
            {
                //SnakeArray.pop();
                //var deletSpriteSnakeCell=SnakeArray.pop();
                deletSpriteSnakeCell=SnakeArray.pop();
                deletDragonCell=SnakeArrayTwo.pop();
                this.removeChildByTag(deletSpriteSnakeCell.getTag(),true);
                this.removeChildByTag(deletDragonCell.getTag(),true);
                //SnakeArray.pop();
                //SnakeArray[j].removeChildByTag(spriteSnakeCell.getTag(),true);
            
                
            }
            cc.log("length aftr dlt"+SnakeArray.length);
                //this.pause();
        }
        if(SnakeArray.length<3)
        {
            this.pause();
            GameOverLabel.setVisible(true);
            return;
        }
        //this.unschedule(this.BombCount);
        this.unschedule(this.correction);
        
    },
    createFood:function() {   
        //Check if food Exist , remove it from the game sprite
        r=this.getRandomArbitrary(1,3);
        if(this.getChildByTag(Enum.snakefood)!=null)
        {
            this.removeChildByTag(Enum.snakefood,true); 
        }       
        var spriteSnakeFood = new SpriteSnakeFood("res/p"+r+".png");
        spriteSnakeFood.setTag(Enum.snakefood); 
        spriteSnakeFood.setScale(0.7);    
        this.addChild(spriteSnakeFood,3); 
        var x=this.getRandomArbitrary(0,11);
        var y=this.getRandomArbitrary(0,11);
        spriteSnakeFood.setPosition((initialx+(y*50)-(x*25)),(initialy+(y*12)+(x*40)));
        SnakeFood = {
            x: (initialx+(y*50)-(x*25)) , 
            y: (initialy+(y*12)+(x*40))  
        };

        for(i=0;i<SnakeArray.length;i++)
        {
            var xxxxxx=SnakeArray[i].x;
            var yyyyyy=SnakeArray[i].y;
            if(xxxxxx==SnakeFood.x&&yyyyyy==SnakeFood.y)
            {
                cc.log("change");
                var x=this.getRandomArbitrary(0,11);
                var y=this.getRandomArbitrary(0,11);
                spriteSnakeFood.setPosition((initialx+(y*50)-(x*25)),(initialy+(y*12)+(x*40)));
                SnakeFood = {
                    x: (initialx+(y*50)-(x*25)) , 
                    y: (initialy+(y*12)+(x*40))  
                };
            }
        }
       // spriteSnakeFood.x = x; 
       // spriteSnakeFood.y = y; 
    },
    createCharacter:function()
    {
        //charecter=true;
        if(this.getChildByTag(Enum.snakecharecter)!=null)
        {
            this.removeChildByTag(Enum.snakecharecter,true); 
        }       
        var spriteSnakeCharecter = new SpriteSnakeFood("res/dragon.png");
        spriteSnakeCharecter.setTag(Enum.snakecharecter);     
        this.addChild(spriteSnakeCharecter,3); 
        var x=this.getRandomArbitrary(0,11);
        var y=this.getRandomArbitrary(0,11);
        spriteSnakeCharecter.setPosition((initialx+(y*50)-(x*25)),(initialy+(y*12)+(x*40)));
        SnakeCharecter = {
            x: (initialx+(y*50)-(x*25)) , 
            y: (initialy+(y*12)+(x*40))  
        };
        for(i=0;i<SnakeArray.length;i++)
        {
            var xxxxxx=SnakeArray[i].x;
            var yyyyyy=SnakeArray[i].y;
            if(xxxxxx==SnakeCharecter.x&&yyyyyy==SnakeCharecter.y)
            {
                cc.log("change");
                var x=this.getRandomArbitrary(0,11);
                var y=this.getRandomArbitrary(0,11);
                spriteSnakeCharecter.setPosition((initialx+(y*50)-(x*25)),(initialy+(y*12)+(x*40)));
                SnakeCharecter = {
                    x: (initialx+(y*50)-(x*25)) , 
                    y: (initialy+(y*12)+(x*40))  
                };
            }
        }

    },
    createSecondFood:function()
    {
        
        if(flag)
        {
            flag=false;
            this.schedule(this.createFood,6);
        }
        setTimeout(function(){
            flag=true;
        },6000);
    },
    gameLoop:function()
    {
        SnakeHeadX = SnakeArray[0].x;
        SnakeHeadY = SnakeArray[0].y;
        switch(direction)
        {
            case "right":
                SnakeHeadX+=50;
                SnakeHeadY+=(12);
                d=1;
                jumpright++;
                jumpleft--;

            break;
            case "left":
                SnakeHeadX-=(50);
                SnakeHeadY-=(12);
                d=4;
                jumpleft++;
                jumpright--;
            break;
            case "up":
                SnakeHeadX-=25;
                SnakeHeadY+=40;//7 
                d=2;
                jumpup++;
                jumpdown--;
            break;
            case "down":
                SnakeHeadX+=25;
                SnakeHeadY-=40;
                d=3;
                jumpdown++;
                jumpup--;

            break;
            default:
                cc.log("direction not defind");
            
        }
        if(CollisionDetector(SnakeHeadX,SnakeHeadY,SnakeArray))
        {
            this.pause();
            GameOverLabel.setVisible(true);
            return;
        }
        if(SnakeHeadX==SnakeFood.x&&SnakeHeadY==SnakeFood.y)
        {
            cc.log("found");
            this.removeChildByTag(Enum.snakefood,true);
            if(r==2)
                {
                    var w=this.getRandomArbitrary(1,5);
                    if(w==1)
                    {
                        food+=25;
                    }
                    else if(w==2)
                    {
                        food+=50;
                    }
                    else if(w==3)
                    {
                        food+=75;
                    }
                    else
                    {
                        food+=100;
                    }
                    FoodLabel.setString("Food : "+(food));
                }
                else
                {
                    var l=this.getRandomArbitrary(1,5);
                    if(l==1)
                    {
                        coin+=25;
                    }
                    else if(l==2)
                    {
                        coin+=50;
                    }
                    else if(l==3)
                    {
                        coin+=75;
                    }
                    else
                    {
                        coin+=100;
                    }
                    CoinLabel.setString("Coin : "+(coin));
                    
                }
            if(SnakeArray.length<5)
            {
                this.createFood();
            }
            else
            {
                this.createSecondFood();
            }
        }
        if(bombCreated)
        {
            if(SnakeHeadX==Bomb.x&&SnakeHeadY==Bomb.y)
            {
                bombCreated=false;
                var p= new Date();
                touchTime=p.getSeconds();
                cc.log("touch time"+touchTime);
                if(touchTime<startTime)
                {
                    touchTime=touchTime+60;
                }
                this.scheduleOnce(this.BombCount);
            }
        }
        
        if(SnakeHeadX==SnakeCharecter.x&&SnakeHeadY==SnakeCharecter.y)
        {
            var spriteSnaketail=new SpriteSnakeCell("res/red.png");
            spriteSnaketail.setTag(Enum.snakecell);
            this.addChild(spriteSnaketail);
            spriteSnaketail.x = SnakeHeadX; 
            spriteSnaketail.y = SnakeHeadY;
            SnakeArray.unshift(spriteSnaketail);


            var changeImageOfSnake=new SpriteSnakeCell("res/d"+d+".png");
            changeImageOfSnake.x = SnakeHeadX; 
            changeImageOfSnake.y = SnakeHeadY;
            this.addChild(changeImageOfSnake,2);
            changeImageOfSnake.setTag(Enum.dragoncell);
            SnakeArrayTwo.unshift(changeImageOfSnake);

            this.createCharacter();

        }
        else
        {
            var spriteSnakeCellLast = SnakeArray.pop(); 
            var changeImageOfSpriteCell=new SpriteSnakeCell("res/red.png");
            changeImageOfSpriteCell.x=SnakeHeadX;
            changeImageOfSpriteCell.y=SnakeHeadY;
            this.addChild(changeImageOfSpriteCell,2);
            changeImageOfSpriteCell.setTag(Enum.snakecell);
            SnakeArray.unshift(changeImageOfSpriteCell);
            this.removeChildByTag(spriteSnakeCellLast.getTag(),true);
            spriteDragonCellLast=SnakeArrayTwo.pop();
            //cc.log(spriteDragonCellLast.x,spriteDragonCellLast.y);
            this.removeChildByTag(spriteDragonCellLast.getTag(),true);
            var changeImageOfSnake=new SpriteSnakeCell("res/d"+d+".png");
            changeImageOfSnake.x = SnakeHeadX; 
            changeImageOfSnake.y = SnakeHeadY;
            this.addChild(changeImageOfSnake,2);
            changeImageOfSnake.setTag(Enum.dragoncell);
            SnakeArrayTwo.unshift(changeImageOfSnake);
        }
    }

});
function CollisionDetector(snakeHeadX,snakeHeadY,snakeArray)
{
        if(jumpright<0||jumpright>11||jumpleft>11||jumpleft<0||jumpup>11||jumpup<1||jumpdown>11||jumpdown<1)
        {
            return true;
        }
        for(var i = 0; i < snakeArray.length; i++)
        {
            if(snakeArray[i].x == snakeHeadX && snakeArray[i].y == snakeHeadY)
            {
                return true;
            }
        }
        return false;
}

var HelloWorldScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new HelloWorldLayer();
        this.addChild(layer);
    }
});

